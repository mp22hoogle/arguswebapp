﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Argus.Models;

namespace Argus.Controllers
{
    public class ArgusController : Controller
    {
        // GET: Argus
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ErrorLabel = "";
            return View();
        }

        [HttpPost]
        public ActionResult Index(Login lng)
        {
            if (lng.Password == null)
            {
                ViewBag.ErrorLabel = "Please Enter Administrator Password";
                return View();
            }
            if (lng.Password.Trim() != "1234")
            {
                ViewBag.ErrorLabel = "Administrator Password is incorrect";
                return View();
            }
            return RedirectToAction("Home");
        }

        public ActionResult Home()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }

        public ActionResult Calculator()
        {
            return View();
        }

        public ActionResult Graph_StudentsbyMajor()
        {
            return View();
        }

        public ActionResult Graph_StudentsbyStates()
        {
            return View();
        }

        public ActionResult Graph_StudentsFieldDistribution()
        {
            return View();
        }

        public ActionResult Graph_StudentsbyResidencyType()
        {
            return View();
        }

        public ActionResult Graph_EmploymentProjection()
        {
            return View();
        }

        public ActionResult Graph_EmploymentAccordingtoIndustry()
        {
            return View();
        }
    }
}